From 3e0222fcae552685d423914a683c1709dc5f6d6b Mon Sep 17 00:00:00 2001
From: Peter Hutterer <peter.hutterer@who-t.net>
Date: Mon, 27 Nov 2023 16:27:49 +1000
Subject: [PATCH xserver] randr: avoid integer truncation in length check of
 ProcRRChange*Property

Affected are ProcRRChangeProviderProperty and ProcRRChangeOutputProperty.
See also xserver@8f454b79 where this same bug was fixed for the core
protocol and XI.

This fixes an OOB read and the resulting information disclosure.

Length calculation for the request was clipped to a 32-bit integer. With
the correct stuff->nUnits value the expected request size was
truncated, passing the REQUEST_FIXED_SIZE check.

The server then proceeded with reading at least stuff->num_items bytes
(depending on stuff->format) from the request and stuffing whatever it
finds into the property. In the process it would also allocate at least
stuff->nUnits bytes, i.e. 4GB.

CVE-2023-XXXXX, ZDI-CAN-22561

This vulnerability was discovered by:
Jan-Niklas Sohn working with Trend Micro Zero Day Initiative
---
 randr/rrproperty.c         | 2 +-
 randr/rrproviderproperty.c | 2 +-
 2 files changed, 2 insertions(+), 2 deletions(-)

--- a/randr/rrproperty.c
+++ b/randr/rrproperty.c
@@ -530,7 +530,7 @@ ProcRRChangeOutputProperty(ClientPtr cli
     char format, mode;
     unsigned long len;
     int sizeInBytes;
-    int totalSize;
+    uint64_t totalSize;
     int err;
 
     REQUEST_AT_LEAST_SIZE(xRRChangeOutputPropertyReq);
--- a/randr/rrproviderproperty.c
+++ b/randr/rrproviderproperty.c
@@ -498,7 +498,7 @@ ProcRRChangeProviderProperty(ClientPtr c
     char format, mode;
     unsigned long len;
     int sizeInBytes;
-    int totalSize;
+    uint64_t totalSize;
     int err;
 
     REQUEST_AT_LEAST_SIZE(xRRChangeProviderPropertyReq);
